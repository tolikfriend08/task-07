package com.epam.rd.java.basic.task7.db.entity;

import java.io.Serializable;
import java.util.Objects;

//for web app use serializable entity
//public class User implements Serializable {
public class User{

	private int id;

	private String login;

	public User() {
	}

	public User(int id, String login) {
		this.id = id;
		this.login = login;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
//		return new User(0,login);
		User user = new User();
		user.setId(0);
		user.setLogin(login);
		return user;
	}


	@Override
	public String toString() {
		return login;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
//		return id == user.id && login.equals(user.login);
		return login.equals(user.login);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, login);
	}
}
