package com.epam.rd.java.basic.task7.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

    private static DBManager instance;

    //singleton
    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    ////////////////////////////////////////

    //to use logging add maven dependency log4j
//    private static final Logger LOG = Logger.getLogger(DBManager.class.getName());


//    private static final String URL = "jdbc:mysql://10.7.0.9:3307/testdb?user=testuser&password=testpass";
//    private static final String FULL_URL = "jdbc:mysql://localhost:3306/testdb?user=root&password=";


    private static final File fileWithUrl = new File("app.properties");
    private static final String FULL_URL = readResourcesFromFile(fileWithUrl);


    private static String readResourcesFromFile(File file) {
        String result = null;
        try {
            Scanner myReader = new Scanner(file);
            result = myReader.nextLine().replaceAll("connection.url=", "");
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Method that finds all users in database and place them to list
     *
     * @return - list of users
     * @throws DBException - if a database access error occurs or the url is null
     */
    public List<User> findAllUsers() throws DBException {
        List<User> usersList = new ArrayList<>();

        Statement stmt = null;
        ResultSet rs = null;
        Connection connection = null;

        //connection вызывается отдельно в каждом методе для возможности работать многопоточно и одновременно обрабатывать множество запросов,
        //создавая множество connection
        //после завершения выполнения блока try объекты connection, statement, resultSet надо закрыть методом close()
        //вызывая метод connection.close() statement & resultSet закрываются автоматически
        //чтобы не вызывать метод close() объекты можно поместить в () блока try
        try {
            connection = DriverManager.getConnection(FULL_URL);
            stmt = connection.createStatement();
            rs = stmt.executeQuery(SQLQueryConstant.FIND_ALL_USERS);
            while (rs.next()) {
//                User user = new User();
//                user.setId(rs.getInt(1));//для derby работает только columnIndex, NOT columnName
//                user.setLogin(rs.getString(2));
//                usersList.add(user);
                usersList.add(new User(rs.getInt(1), rs.getString(2)));//!!!!!метод rs.getNString() не работает с derby!!!!!   use ONLY rs.getString()
            }
        } catch (SQLException e) {
            e.printStackTrace();
//            LOG.error(Messages.ERR_CANNOT_OBTAIN_USER_ORDER_BEANS, ex);
            throw new DBException(DBException.LIST_OF_USERS_NOT_CREATED, e);
        } finally {
            DBUtil.close(connection, stmt, rs);
        }
        return usersList;
    }


    /**
     * Method to insert new user to database
     *
     * @param user to be inserted to database
     * @return true - if user inserted to database, false otherwise
     * @throws DBException - if a database access error occurs or the url is null or user already exist in database
     */
    public boolean insertUser(User user) throws DBException {
        int result = 0;
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = DriverManager.getConnection(FULL_URL);
            stmt = con.prepareStatement(SQLQueryConstant.INSERT_USER, Statement.RETURN_GENERATED_KEYS);//RETURN_GENERATED_KEY возвращает автоинкрементируемое значение, приссвоенное в бд
            stmt.setString(1, user.getLogin());
            result = stmt.executeUpdate();
            if (result > 0) {
                ResultSet rs = stmt.getGeneratedKeys();//get user_id from database generated for current user
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                }
            }

        } catch (SQLException e) {
//            e.printStackTrace();
            throw new DBException(DBException.USER_NOT_INSERTED, e);
        } finally {
            DBUtil.close(stmt);
            DBUtil.close(con);
        }
        return result > 0;
    }


    /**
     * @param users to be deleted from database
     * @return true if all users are existed in database and has been successfully deleted, false otherwise
     * @throws DBException if a database access error occurs or the url is null or user not exist in database
     */
    public boolean deleteUsers(User... users) throws DBException {
        int result = 0;
        Connection connection = null;
        PreparedStatement stmt = null;

        try {
            connection = DriverManager.getConnection(FULL_URL);
            for (int i = 0; i < users.length; i++) {
                stmt = connection.prepareStatement(SQLQueryConstant.DELETE_USERS);
                stmt.setString(1, users[i].getLogin());
                result += stmt.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            //some log
            throw new DBException(DBException.USERS_NOT_DELETED, e);
        } finally {
            DBUtil.close(stmt);
            DBUtil.close(connection);
        }

        return result > 0;
    }

    /**
     * Return the user from the database if exist using user login
     *
     * @param login of the user to return
     * @return user if exist
     * @throws DBException if a database access error occurs or the url is null
     */
    public User getUser(String login) throws DBException {
        User user = new User();
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            connection = DriverManager.getConnection(FULL_URL);
            stmt = connection.prepareStatement(SQLQueryConstant.FIND_USER);
            stmt.setString(1, login);
            rs = stmt.executeQuery();

            if (rs.next()) {
                user.setId(rs.getInt(1));
                user.setLogin(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //Log has to be added here
            throw new DBException(DBException.USER_NOT_FOUND, e);
        } finally {
            DBUtil.close(connection, stmt, rs);
        }

        return user;
    }

    /**
     * Return the team from the database if exist using team name
     *
     * @param name of the team to return
     * @return team if exist
     * @throws DBException if a database access error occurs or the url is null
     */
    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            connection = DriverManager.getConnection(FULL_URL);
            stmt = connection.prepareStatement(SQLQueryConstant.FIND_TEAM);
            stmt.setString(1, name);
            rs = stmt.executeQuery();

            if (rs.next()) {
                team.setId(rs.getInt(1));
                team.setName(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //Log has to be added here
            throw new DBException(DBException.TEAM_NOT_FOUND, e);
        } finally {
            DBUtil.close(connection, stmt, rs);
        }

        return team;
    }


    /**
     * Method that finds all teams in database and place them to list
     *
     * @return - list of teams
     * @throws DBException if a database access error occurs or the url is null
     */
    public List<Team> findAllTeams() throws DBException {
        List<Team> teamsList = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(FULL_URL);
             Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery(SQLQueryConstant.FIND_ALL_TEAMS)) {
            while (rs.next()) {
                teamsList.add(new Team(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //Log has to be added here
            throw new DBException(DBException.LIST_OF_TEAMS_NOT_CREATED, e);
        }
        return teamsList;
    }


    /**
     * Method to insert new team to database
     *
     * @param team to be inserted to database
     * @return true - if team inserted to database, false otherwise
     * @throws DBException - if a database access error occurs or the url is null or team already exist in database
     */
    public boolean insertTeam(Team team) throws DBException {
        int result = 0;
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = DriverManager.getConnection(FULL_URL);
            stmt = con.prepareStatement(SQLQueryConstant.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, team.getName());
            result = stmt.executeUpdate();
            if (result > 0) {
                ResultSet rs = stmt.getGeneratedKeys();//get team_id from database generated for current team
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //some log
            throw new DBException(DBException.TEAM_NOT_INSERTED, e);
        } finally {
            DBUtil.close(stmt);
            DBUtil.close(con);
        }
        return result > 0;
    }

    /**
     * Method for adding user to teams
     *
     * @param user  - to be added to teams
     * @param teams - array of teams to which is user added
     * @return true if user added to all teams from array inserted to database, false otherwise
     * @throws DBException - if a database access error occurs or the url is null or the transaction is failed
     */
    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        int result = 0;
        Connection connection = null;
        PreparedStatement stmt = null;

        try {
            connection = DriverManager.getConnection(FULL_URL);
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);//невозможность считывать добавленные в бд данные, которые не подтверждены вызовом метода commit()
            for (int i = 0; i < teams.length; i++) {
                stmt = connection.prepareStatement(SQLQueryConstant.SET_TEAMS_FOR_USER);
                int k = 1;
                stmt.setInt(k++, user.getId());
                stmt.setInt(k++, teams[i].getId());
                result += stmt.executeUpdate();
            }

            DBUtil.commit(connection);

        } catch (SQLException e) {
            DBUtil.rollback(connection);
            e.printStackTrace();
            //some log
            throw new DBException(DBException.USER_NOT_ADDED_TO_TEAMS, e);
        } finally {
            DBUtil.close(stmt);
            DBUtil.close(connection);
        }
        return result > 0;
    }

    /**
     * Method that finds all teams for which user is assigned
     *
     * @param user - which teams is looking for
     * @return - list of teams
     * @throws DBException - if a database access error occurs or the url is null
     */
    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teamsList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {

            connection = DriverManager.getConnection(FULL_URL);
            stmt = connection.prepareStatement(SQLQueryConstant.FIND_ALL_TEAMS_OF_USER);
            stmt.setInt(1, user.getId());
            rs = stmt.executeQuery();

            while (rs.next()) {
                teamsList.add(new Team(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //Log has to be added here
            throw new DBException(DBException.LIST_OF_TEAMS_NOT_CREATED, e);
        } finally {
            DBUtil.close(connection, stmt, rs);
        }

        return teamsList;
    }

    /**
     * Method to delete team from database using team name
     *
     * @param team to be deleted
     * @return true if deletion is success, false otherwise
     * @throws DBException if a database access error occurs or the url is null
     */
    public boolean deleteTeam(Team team) throws DBException {
        int result = 0;
        Connection connection = null;
        PreparedStatement stmt = null;

        try {
            connection = DriverManager.getConnection(FULL_URL);
            stmt = connection.prepareStatement(SQLQueryConstant.DELETE_TEAM);
            stmt.setString(1, team.getName());
            result = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            //some log
            throw new DBException(DBException.TEAM_NOT_DELETED, e);
        } finally {
            DBUtil.close(stmt);
            DBUtil.close(connection);
        }
        return result > 0;
    }

    /**
     * Method for update a team
     *
     * @param team to be updated
     * @return true if team has been updated, false otherwise
     * @throws DBException if a database access error occurs or the url is null or team is not exist
     */
    public boolean updateTeam(Team team) throws DBException {
        int result = 0;
        Connection connection = null;
        PreparedStatement stmt = null;

        try {
            connection = DriverManager.getConnection(FULL_URL);
            stmt = connection.prepareStatement(SQLQueryConstant.UPDATE_TEAM);
            stmt.setString(1, team.getName());
            stmt.setInt(2, team.getId());
            result = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            //some log
            throw new DBException(DBException.TEAM_NOT_UPDATED, e);
        } finally {
            DBUtil.close(stmt);
            DBUtil.close(connection);
        }
        return result > 0;
    }


}
